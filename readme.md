# Usage

volume
/data/db


For replication, use the following command

```
mongod --replSet rs_0  --keyFile /usr/local/etc/mongo.key --wiredTigerCacheSizeGB 2

# eg. 
mongod --replSet medicrs0  --keyFile /usr/local/etc/mongo.key --wiredTigerCacheSizeGB 2
```
