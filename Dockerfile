FROM mongo:4.4
LABEL maintainer="Krerk Piromsopa, Ph.D. <krerk.p@chula.ac.th>"

COPY cluster-entrypoint.sh /usr/local/bin/cluster-entrypoint.sh
RUN ln -s usr/local/bin/cluster-entrypoint.sh / # backwards compat

COPY init-replicaset.sh /usr/local/bin/init-replicaset.sh
RUN ln -s usr/local/bin/init-replicaset.sh / # backwards compat

COPY --chown=mongodb:mongodb  mongo.key /usr/local/etc/mongo.key
RUN chmod 600 /usr/local/etc/mongo.key

ENTRYPOINT ["/usr/local/bin/cluster-entrypoint.sh"]

CMD ["mongod"]
