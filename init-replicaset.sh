#!/bin/bash


#dbPath=/data/db
#
#if [ -e "$dbPath/WiredTiger" ];
#then
#	echo =======================================
#	echo "This is an existing cluster."
# 	echo "Skip cluster init."
#	echo =======================================
#	exit;
#fi
#

master=`echo $HOSTNAME | cut -d- -f1`
seq=`echo  $HOSTNAME | cut -d- -f2`

# let's wait until the server is ready.
sleep 30
echo =======================================
echo  bootstrap node as a replica set
echo =======================================
if [ $seq == '0' ]
then
	echo "Node 0"
	echo "  run rs.initiate()"
	mongo --authenticationDatabase admin --username $MONGO_INITDB_ROOT_USERNAME -p$MONGO_INITDB_ROOT_PASSWORD --eval "rs.initiate()"
	mongo --authenticationDatabase admin --username $MONGO_INITDB_ROOT_USERNAME -p$MONGO_INITDB_ROOT_PASSWORD --eval "config=rs.config();config.members[0].host='$master-$seq.$master';rs.reconfig(config)"
else
	echo "Node $seq"
	mongo --authenticationDatabase admin --host $master-0.$master --username $MONGO_INITDB_ROOT_USERNAME -p$MONGO_INITDB_ROOT_PASSWORD --eval "rs.add( { host: '$master-$seq.$master', priority: 0, votes: 0 } )"

fi
echo =======================================
echo  replica set should be ready.
echo =======================================

