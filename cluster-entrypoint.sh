#!/bin/bash


set -eo pipefail
shopt -s nullglob

# if command starts with an option, prepend mongod
if [ "${1:0:1}" = '-' ]; then
	set -- mongod "$@"
fi

# begin: added by krerk
echo Deverhood MongoDB Cluster configuration
echo =======================================
echo "Hostname - $HOSTNAME"

master=`echo $HOSTNAME | cut -d- -f1`
seq=`echo  $HOSTNAME | cut -d- -f2`

if [ $seq = $master ]
then
	echo "  Cannot detect [host]-[seq]"
	echo "  Assume standalone server mode"
	
	echo "$@"
	exec docker-entrypoint.sh "$@"
	exit 0
fi

echo Master is $master-0.$master
echo node seq - $seq

init-replicaset.sh &

echo start mongod with "$@"
exec docker-entrypoint.sh "$@"
